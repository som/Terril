Terril mostly uses [Mint-Y-Dark theme](https://github.com/linuxmint/mint-y-theme) colors. That means pure greys and a bit of green.

Gtk4 theme is based on upstream Gtk 'Default' theme.

Gtk3 theme is based on [Arc-Dark theme](https://github.com/horst3180/arc-theme) and is very similar to Mint-Y-Dark, differing by window controls, transparency and a few details.

Gtk2 theme is just duplicated from [Mint-Y-Dark theme](https://github.com/linuxmint/mint-y-theme/tree/master/usr/share/themes/Mint-Y-Dark/gtk-2.0).

Gnome-shell theme is based on upstream "default" theme.

Firefox theme with tabs at the bottom:

![](screenshots/firefox-tabs-bottom-screenshot.png)
