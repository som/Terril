[gnome-shell.css](gnome-shell.css) is [gnome-shell-base.css](gnome-shell-base.css) plus some tweaks.

[gnome-shell-base.css](gnome-shell-base.css) uses [gnome-shell-sass](gnome-shell-sass) (which is nothing more than a copy of [upstream gnome-shell-sass](https://gitlab.gnome.org/GNOME/gnome-shell/tree/master/data/theme/gnome-shell-sass)) and a [custom _colors.scss](_customColors.scss).
It is generated via [gnome-shell-base.scss](gnome-shell-base.scss) with the command line:
`sassc gnome-shell-base.scss gnome-shell-base.css`

Unless you want to change global colors, [gnome-shell.css](gnome-shell.css) is the right place to easily improve the theme, because [gnome-shell-base.css](gnome-shell-base.css) is just the upstream gnome-shell theme with pure grey background colors (#404040 and #2f2f2f instead of #393f3f and #2e3436).

To update the theme, paste upstream 'gnome-shell-sass' inside this folder and run `gen.sh`.
