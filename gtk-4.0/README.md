Base on GTK4 'Default theme'.

To update the theme:
* Paste upstream GTK theme inside the `Default` directory.
* Paste upstream Adwaita theme inside the `Adwaita` directory without removing the `_drawing.scss` link.
* Run `gen.sh`.
